<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // user gates
        Gate::define('store-update-destroy-users', function (User $user) {
            return $user->role === 'admin';
        });
        Gate::define('index-show-users', function (User $user) {
            return in_array($user->role, ['operator', 'admin']);
        });

        // delivery gates
        Gate::define('store-destroy-deliveries', function (User $user) {
            return $user->role === 'delivery_service';
        });
    }
}
