<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Resources\Delivery as DeliveryResource;
use Exception;

class DeliveryController extends Controller
{
    public function __construct()
    {
        $this->statuses = ['created', 'placed', 'issued'];
        $this->rules = [
            'content_name' => 'required|string',
            'status' => ['required', Rule::in($this->statuses)]
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->sendResponse(
            DeliveryResource::collection(Delivery::all()),
            'Список отправлений успешно получен.'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->authorize('store-destroy-deliveries');

        $input = $request->all()+['status' => 'created'];
        $validator = Validator::make($input, $this->rules);

        if($validator->fails()){
            return $this->sendError(
                'Ошибка валидации',
                $validator->errors(),
                403
            );
        }

        $delivery = Delivery::create($input);

        return $this->sendResponse(
            new DeliveryResource($delivery),
            'Отправление успешно создано!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $delivery = Delivery::find($id);
        if (is_null($delivery)) {
            return $this->sendError('Отправление не найдено!');
        }

        return $this->sendResponse(new DeliveryResource($delivery), 'Отправление найдено.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $delivery = Delivery::find($id);

        if(is_null($delivery)){
            return $this->sendError('Отправление не найдено!');
        }

        $input = $request->all();

        $validator = Validator::make($input, $this->rules);

        if($validator->fails()){
            return $this->sendError(
                'Ошибка валидации',
                $validator->errors(),
                403
            );
        }

        $delivery->update($input);

        return $this->sendResponse(new DeliveryResource($delivery), 'Отправление успешно обновлено.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('store-destroy-deliveries');

        $delivery = Delivery::find($id);

        if(is_null($delivery)){
            return $this->sendError('Отправление не найдено!');
        }

        $delivery->delete();
        return $this->sendResponse([], 'Отправление успешно удалено.');
    }
}
