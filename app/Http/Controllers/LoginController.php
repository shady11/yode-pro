<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if(Auth::attempt($request->only('email', 'password'))){
            $user = Auth::user();
            $success['token'] = $user->createToken('auth_token')->plainTextToken;
            $success['name'] = $user->name;

            return $this->sendResponse($success, 'Пользователь успешно авторизовался.');
        }
        else{
            return $this->sendError('Ошибка авторизации.',null, 401);
        }
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        $success['message'] = 'Вы успешно вышли';
        return $this->sendResponse([], 'Вы успешно вышли.');
    }
}
