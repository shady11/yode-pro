<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as UserResource;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->roles = ['admin', 'operator', 'delivery_service'];
        $this->rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'role' => ['required', Rule::in($this->roles)]
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $this->authorize('index-show-users');

        return $this->sendResponse(
            UserResource::collection(User::all()),
            'Список пользователей успешно получен.'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->authorize('store-update-destroy-users');

        $input = $request->all();
        $validator = Validator::make($input, $this->rules);

        if($validator->fails()){
            return $this->sendError(
                'Ошибка валидации',
                $validator->errors(),
                403
            );
        }

        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        return $this->sendResponse(
            new UserResource($user),
            'Пользователь успешно создан!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  integer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $this->authorize('index-show-users');

        $user = User::find($id);
        if (is_null($user)) {
            return $this->sendError('Пользователь не найден!');
        }

        return $this->sendResponse(new UserResource($user), 'Пользователь найден.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('store-update-destroy-users');

        $input = $request->all() + ['old_role' => $user->role];

        $validator = Validator::make($input, [
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id)
            ],
            'role' => 'required|same:old_role',
        ] + $this->rules);

        if($validator->fails()){
            return $this->sendError(
                'Ошибка валидации',
                $validator->errors(),
                403
            );
        }

        $input['password'] = bcrypt($input['password']);
        $user->update($input);

        return $this->sendResponse(new UserResource($user), 'Пользователь успешно обновлен.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $this->authorize('store-update-destroy-users');

        $user->delete();
        return $this->sendResponse([], 'Пользователь успешно удален.');
    }
}
